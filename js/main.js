//===========================
//NAVBAR
//==========================
$(window).scroll(function() {
	"use strict";
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
});


//===========================
//hover thumbnail
//==========================
$('.thumbnails').hover(
        function(){
            $(this).find('.img-caption').fadeToggle(250); //.fadeIn(250)
        }
    );
//===========================
//hover thumbnail end
//==========================
